<?php
$NB_POST = 2;
$NB_SEARCH = 3;

$user = array (
    "id"         => "1",
    "firstname"  => "Amine",
    "lastname"   => "Daï",
    "mail"       => "amine.dai@free.fr",
    "password"   => "azerty",
    "picture"    => "images/pp/test.jpg",
    "class"      => "13/14 CSIA",
    "location"   => "Lille, France",
    "biography"  => "Bonjour moi c'est Amine! :)",
    "facebook"   => "http://facebook.com/wlalele/",
    "twitter"    => "http://twitter.com/wlalelee/",
    "google"    => "https://plus.google.com/u/0/100155209155532380784/posts/"
);
$post = array (
    "id"         => "1",
    "author_id"  => "1",
    "category"   => "Entraide",
    "title"      => "Bienvenue sur Orbe !",
    "content"    => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit nibh ut elit dapibus imperdiet. Maecenas augue orci, semper eget metus eget, aliquam fringilla leo. In vitae eros elementum, dignissim dui nec, sagittis risus. Ut nec nulla felis. Nulla et commodo turpis. Aliquam non ante justo. Ut adipiscing egestas diam. Quisque congue erat lectus, id lobortis justo porttitor eget. Vivamus tincidunt et lacus at tempor. Praesent quis viverra justo. Curabitur convallis nulla non odio faucibus sodales. Ut sagittis mauris sed diam vehicula sagittis. Mauris adipiscing bibendum ipsum in tincidunt. Nullam eu cursus libero."
);
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Orbe</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

    <!-- Chargement Bootstrap -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Chargement Flat UI -->
    <link href="css/flat-ui.css" rel="stylesheet">

    <!-- Chargement Style Orbe -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Chargement de la favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="blur-bg">

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 550px; margin: 100px auto;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h6 class="modal-title">Profil</h6>
                </div>
                <div class="modal-body" style="background: #DEDEDE; padding: 10px; text-align: center;">
                    <a class="th" href="#" style="border-radius: 50%;">
                        <img src="<?php echo $user["picture"]; ?>" width="120px" style="border-radius:50%;">
                    </a>
                    <h5 style="margin-bottom: 0px; margin-top: 2px;">
                        <?php echo $user["firstname"]." ".$user["lastname"]; ?>
                    </h5>
                    <p style="font-size: small; margin-bottom: 5px;">
                        <?php echo $user["location"]; ?>
                    </p>
                    <p style="margin-bottom: 15px;">
                        <?php echo $user["biography"]; ?>
                    </p>
                    <div>
                        <a href="<?php echo $user['facebook']; ?>" target="_blank" class="btn btn-info btn-embossed" style="background-color:#3b5998; width: 110px;">
                            <span class="fui-facebook"></span>
                            Facebook
                        </a>
                        <a href="<?php echo $user['twitter']; ?>" target="_blank" class="btn btn-info btn-embossed" style="background-color:#55acee; width: 110px;">
                            <span class="fui-twitter"></span>
                            Twitter
                        </a>
                        <a href="<?php echo $user['google']; ?>" target="_blank" class="btn btn-info btn-embossed" style="background-color:#dd4b39; width: 110px;">
                            <span class="fui-google"></span>
                            Google
                        </a>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 0px;">
                    <button type="button" class="btn btn-default btn-embossed">
                        <span class="glyphicon glyphicon-user"></span>
                        Consulter son profil
                    </button>
                    <button type="button" class="btn btn-primary btn-embossed">
                        <span class="fui-mail"></span>
                        Contactez le!
                    </button>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-inverse square" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="#" style="vertical-align: top;">
                <span class="glyphicon glyphicon-globe"></span>
                 Orbe
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-01">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#fakelink" style="vertical-align: top;">
                        <span class="glyphicon glyphicon-home"></span>
                         Accueil
                    </a>
                </li>
                <li>
                    <a href="#fakelink" style="vertical-align: top;">
                        <span class="glyphicon glyphicon-usd"></span>
                         Bons plans
                    </a>
                </li>
            </ul>
            <div class="navbar-form navbar-right">
                <a href="#" class="btn btn-primary btn-embossed"><span class="fui-new"></span> Post</a>
            </div>
            <form class="navbar-form navbar-right" action="#" role="search">
                <div class="form-group">
                    <div class="input-group">
                        <input class="form-control" id="recherche" type="search" placeholder="Recherche">
                        <span class="input-group-btn">
                            <button type="submit" class="btn"><span class="fui-search"></span></button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </nav>

    <div class="container">

        <div class="col-md-4">

                <div class="row panel no-padding">
                    <div class="panel-body">
                        <div class="col-md-4 col-sm-2 col-xs-3" style="padding-left:0px; padding-right:0px;">
                            <a class="th" href="#">
                                <img src="<?php echo $user["picture"]; ?>" width="100" style="border-radius:50%;">
                            </a>
                        </div>
                        <div class="col-md-8 col-sm-10 col-xs-9" style="padding-right: 0px; padding-left: 10px;">
                            <h6 style="margin:0px;">
                                <?php echo $user["firstname"]." ".$user["lastname"]; ?>
                            </h6>
                            <div class="section-container vertical-nav" data-section data-options="deep_linking: false; one_up: true">                  
                                <p style="font-size:small; margin-bottom: 6px;">
                                    <?php echo $user["class"]; ?>
                                </p>
                                <a href="#" class="btn btn-primary btn-embossed btn-block" style="margin-bottom: 0px;">
                                    <span class="fui-gear"></span>
                                    Modifier mon profil
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: center;">
                        Posts: 2 | Messages: 265 | Orbiz: 2500
                    </div>
                </div>

                <div class="row panel no-padding">
                    <div class="panel-heading">
                        <div class="form-group"  style="margin-bottom:0px;">
                            <div class="input-group">
                                <input class="form-control" id="rechercheProfil" type="search" placeholder="Rechercher quelqu'un">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn"><span class="fui-search"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <ul style="list-style:none; padding-left:0px; margin-bottom:0px;">
                        <?php
                        for ($j = 0; $j < $NB_SEARCH; $j++) {
                        ?>
                            <li>
                                <div class="row" style="padding:8px;">
                                    
                                    <div class="col-md-8 col-sm-10 col-xs-10" style="padding-right: 0px; padding-left: 10px;">
                                        <a href="#">
                                            <img src="<?php echo $user["picture"]; ?>" width="42" style="border-radius:50%; margin-right:5px;">
                                            <?php echo $user["firstname"]." ".$user["lastname"]; ?>
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-4 col-sm-2 col-xs-2 pull-right" style="padding-left:0px; padding-right:0px;">
                                        <button type="submit" class="btn btn-default btn-embossed"><span class="fui-user"></span></button>
                                        <button type="submit" class="btn btn-primary btn-embossed"><span class="fui-mail"></span></button>
                                    </div>
                                </div>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary btn-embossed">
                            Afficher plus de resultat
                            <span class="fui-arrow-right"></span>
                        </button>
                    </div>
                </div>

        </div>
        <div class="decalage col-md-8">
            
            <div class="row panel">
                <div class="form-group"  style="margin-bottom:0px;">
                    <div class="input-group">
                        <input class="form-control" id="postStatus" type="search" placeholder="Publiez une annonce">
                        <span class="input-group-btn">
                            <button type="submit" class="btn"><span class="fui-new"></span></button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row panel no-padding">
                <ul style="list-style:none; padding-left:0px;">
                    <?php
                    for ($i = 0; $i < $NB_POST; $i++) {
                    ?>
                    <li>
                        <div class="row panel-body">
                            <div class="col-md-2 col-sm-2 col-xs-4" style="padding-right: 0px;">
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <img src="<?php echo $user["picture"]; ?>" width="100" style="border-radius: 50%;">
                                </a>
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <strong style="margin-top: 5px;">
                                        <?php echo $user["firstname"]." ".$user["lastname"]; ?>
                                    </strong>
                                </a>
                                <p style="font-size: small;">
                                    Catégorie: 
                                    <?php
                                        echo $post["category"];
                                    ?>
                                </p>

                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-8">
                                <h4>
                                    <?php
                                        echo $post["title"];
                                    ?>
                                </h4>
                                <p style="font-size: medium;">
                                    <?php
                                        echo $post["content"];
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="row panel-footer post-footer">
                            <div class="col-lg-6 col-md-6 col-xs-6">    
                                <p style="font-size: normal;">
                                    Catégorie: 
                                    <?php
                                        echo $post["category"];
                                    ?>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6">
                                <!--<div class="btn-toolbar">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" href="#fakelink">
                                            <span class="fui-heart"></span>
                                        </a>
                                        <a class="btn btn-primary" href="#fakelink">
                                            <span class="glyphicon glyphicon-share"></span>
                                        </a>
                                    </div>
                                </div>-->
                                <button type="submit" class="btn btn-primary btn-embossed pull-right">
                                    Afficher la suite
                                    <span class="fui-arrow-right"></span>
                                </button>
                            </div>
                        </div>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

        </div>

    </div>

    <!-- debut footer -->
    <footer>
    </footer>
    <!-- fin footer -->
    <!-- Chargement des fichiers javascript -->
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/flatui-checkbox.js"></script>
    <script src="js/flatui-radio.js"></script>
    <script src="js/jquery.tagsinput.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/application.js"></script>
  </body>
</html>
