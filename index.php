<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Orbe</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

    <!-- Chargement Bootstrap -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Chargement Flat UI -->
    <link href="css/flat-ui.css" rel="stylesheet">

    <!-- Chargement Style Orbe -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Chargement de la favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="blur-bg">
    <!-- integrer un logo ici -->

    <!-- debut login -->
    <div class="login col-md-push-3 col-md-6 col-sm-12" style="margin-top: 12%;">

      <div class="login-form">

        <div class="form-group">
          <input type="text" class="form-control login-field" value="" placeholder="Email" id="login-name" />
          <label class="login-field-icon fui-user" for="login-name"></label>
        </div>

        <div class="form-group">
          <input type="password" class="form-control login-field" value="" placeholder="Mot de passe" id="login-pass" />
          <label class="login-field-icon fui-lock" for="login-pass"></label>
        </div>

        <div class="col-md-4 col-sm-12" style="padding-left:0px;">
            <label class="checkbox" for="checkbox1">
                <span class="icons">
                    <span class="first-icon fui-checkbox-unchecked"></span>
                    <span class="second-icon fui-checkbox-checked"></span>
                </span>
                <input type="checkbox" value="" id="checkbox1" data-toggle="checkbox">
                Se souvenir de moi
            </label>
        </div>
        <div class="col-md-8 col-sm-12" style="padding:0px;">
            <a class="btn btn-primary btn-lg btn-embossed btn-block" href="home.php">
                Connexion
            </a>
        </div>

        <div class="row">
        <div class="col-md-6 col-sm-12" style="">
            <a class="login-link left" href="#">
                Mot de passe oublié ?
            </a>
        </div>
        <div class="col-md-6 col-sm-12">
            <a class="login-link right" href="register.php">
                Vous n'avez pas de compte ? Inscrivez-vous
            </a>
        </div>
        </div>

    </div>
    <!-- fin login -->
    <!-- debut footer -->
    <footer>
    </footer>
    <!-- fin footer -->
    <!-- Chargement des fichiers javascript -->
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/flatui-checkbox.js"></script>
    <script src="js/flatui-radio.js"></script>
    <script src="js/jquery.tagsinput.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/application.js"></script>
  </body>
</html>
