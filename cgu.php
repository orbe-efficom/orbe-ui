<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Orbe</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=1000, initial-scale=1.0, maximum-scale=1.0">

    <!-- Chargement Bootstrap -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Chargement Flat UI -->
    <link href="css/flat-ui.css" rel="stylesheet">

    <!-- Chargement Style Orbe -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Chargement de la favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="blur-bg">
<!-- integrer un logo ici -->

<!-- debut login -->
<div class="login col-md-push-3 col-md-6 col-sm-12" style="margin-top: 12%;">

    <div class="login-form">
        <article>
            <h2>Conditions générales d’utilisation</h2>

            <h3>I Définitions</h3>

            CGU : désigne les présentes conditions générales d’utilisation du site ORBE<br/>

            Profil Utilisateur : désigne les données personnelles relatives à un Utilisateur, comprenant notamment ses identifiants, dont l’enregistrement est nécessaire pour accéder à l’ensemble des fonctionnalités de www.orbe…….(a prévoir). et au(x) Service(s).<br/>

            Identifiants : désigne l’adresse email, le login, ainsi que tout code confidentiel ou mot de passe choisis pa l’utilisateur et permettant à l’utilisateur de s’identifier afin d’accéder à l’ensemble des fonctionnalités de www.orbe…..(a prévoir).<br/>

            Inscription : désigne la procédure que l’utilisateur doit accomplir sur www.orbe…..(a prévoir) afin de pouvoir accéder au(x) service(s). Cette Inscription conduit à la création des Identifiants et du profil Utilisateur.<br/>

            Service(s) ou Site Web : désigne l’un ou l’ensemble des services fournis par ORBE et accessibles en ligne à partir du site web www.orbe…..(a prévoir).<br/>

            www.orbe…..(a prévoir) : désigne le site Internet, accessible à l’adresse www.orbe…..(a prévoir), permettant d’accéder aux Services soumis aux présentes CGU.<br/>

            Utilisateur : désigne toute personne physique ou morale accédant au(x) Service(s) pour fournir du Contenu et/ou titulaire d’un profil Utilisateur.<br/>

            Membre : Autre terme pour désigner un Utilisateur.<br/>

            Contenu : désigne toute publication faite par un Utilisateur, incluant les informations du Profil Utilisateur, les documents et tout autre texte saisi par l’utilisateur.<br/>

            Document : désigne tout type de fichier publié par un Utilisateur dans un album photos ainsi que l’image d’avatar du Profil Utilisateur.<br/>

            Administrateur : désigne Un Utilisateur pourvus de droits supplémentaire sur le(s) Service(s) su site www.orbe…..(a prévoir), lui permettant d’assurer la modération et l’animation du site www.orbe…..(a prévoir).<br/><br/>


            <h3>II Objet des CGU et version en vigueur</h3>

            Les présentes Conditions Générales d’Utilisation ont pour objet de définir les conditions dans lesquelles les Utilisateurs peuvent accéder aux services et les utiliser.<br/>

            Toute personne qui accéde à l’un des Services proposés par ORBE s’engage à respecter, sans réserve, les présentes Conditions Générales d’Utilisation.<br/>

            Tout Utilisateur est donc tenu de se référer à leur version accessible en ligne à la date de son accès à l’un quelconque des Services. L’Utilisateur est expressément informé que l’unique version des Conditions Générales d’Utilisation des services en lignes de ORBE qui fait foi est celle qui se trouve en ligne sur le site www.orbe…..(a prévoir), ce qu’il reconnait et accepte sans restrictions, s'engageant à s’y référer systématiquement lors de chaque connexion.<br/>

            La version des présentes CGU qui prévaut est celle qui est accessible en ligne à l’adresse suivante : “http.www.orbe…..(a prévoir)”. ORBE est libre de modifier, à tout moment, les présentes CGU, afin notamment de prendre en compte toute évolution légale, jurisprudentielle, éditorial et/ou technique.<br/>

            <h3>III Inscription et accès</h3>

            Modalités générales d'Inscription aux Services en ligne et de création de son Profil Utilisateur
            L'inscription à ORBE est gratuite et peut être à tout moment résiliée sur demande de l'utilisateur.
            Lors de son Inscription en ligne, l'Utilisateur peut utiliser un pseudonyme, cependant il s'engage à ne pas créer une fausse identité de nature à induire ORBE ou les tiers en erreur et à ne pas usurper l'identité d'une autre personne morale ou physique.
            Après validation des CGU et la création de son Profil Utilisateur, l'Utilisateur reçoit un premier e-mail dans sa messagerie personnelle, lui restituant l'adresse mail avec laquelle il s'est inscrit au Service
            Dans l'hypothèse où l'Utilisateur fournirait des informations fausses, inexactes, périmées, incomplètes, trompeuses ou de nature à induire en erreur, ORBE pourra, immédiatement sans préavis ni indemnité, suspendre ou résilier le Profil Utilisateur de l'Utilisateur et lui refuser l'accès, de façon temporaire ou définitive à tout ou partie des Services<br/>

            Modalités particulières d'inscription aux Services par des mineurs
            Il est rappelé aux parents et à toute personne exerçant l'autorité parentale, qu'il leur appartient, d'une part, de déterminer quel(s) Service(s) leur enfant mineur est autorisé à utiliser et, d'autre part, de surveiller l'utilisation que celui-ci fait de ce(s) Service(s).
            Les mineurs sont admis à s'inscrire, à la condition expresse qu'ils aient préalablement obtenu de leurs parents (ou de la personne exerçant l'autorité parentale) l'autorisation de le faire. Le fait pour eux de s'inscrire implique qu'ils ont obtenu cette autorisation préalable. ORBE se réserve le droit d'en demander la justification écrite à tout moment, et de procéder à toutes vérifications, ainsi que de supprimer tout Profil Utilisateur dont le titulaire mineur ne remettrait pas immédiatement ladite justification, ou éventuellement dans le délai qui lui serait imparti. ORBE supprimera immédiatement tout Profil Utilisateur, dès la réception par ses services d'une demande parentale de fermeture du Profil Utilisateur et des Contenus y afférents.<br/>

            <h3>IV Facteurs opérationnels communs</h3>

            Obligation de l'Utilisateur relativement à ses Identifiants et son Compte Personnel
            Le Profil Utilisateur d'un Utilisateur comprend notamment ses Identifiants, confiés par ORBE et placés sous la responsabilité exclusive de l'Utilisateur. L'Utilisateur s'oblige à les conserver secrets et à ne pas les divulguer sous quelque forme que ce soit. Si l'un des éléments d'identification de l'Utilisateur est perdu ou volé, ce dernier doit informer sans délai ORBE, qui procédera alors à l'annulation et/ou la mise à jour immédiate(s) de l'Identifiant concerné.
            L'Utilisateur est seul responsable de l'utilisation qui est faite de ses Identifiants, même si l'Utilisateur pré-enregistre sur son ordinateur, son téléphone mobile ou sur n'importe quel autre type d'équipement son Identifiant, permettant ainsi la connexion automatique aux Services.
            Tout accès, utilisation des Services et transmission de données effectués à partir du Compte d'un Utilisateur sera réputé avoir été effectué par ce dernier. La sauvegarde de la confidentialité du mot de passe confié à l'Utilisateur relève de l'entière responsabilité de ce dernier. A cet égard, l'Utilisateur est tenu de s'assurer qu'à l'issue de chaque session il se déconnecte effectivement des Services, en particulier lorsqu'il accède aux Services à partir d'un ordinateur public.<br/>

            <h3>V Description des services</h3>


            <h3>VI Aspects “Légaux”</h3>

            Limitation de responsabilité
            ORBE ne peut être tenu responsable de l'utilisation qui serait faite des Services par les Utilisateurs. En particulier, ORBE ne peut en aucune façon être responsable au titre des Contenus affichés, mis en ligne, envoyés, reçus ou transmis par les Utilisateurs ou présents sur les Sites Tiers.
            Aucun conseil et aucune information, qu'ils soient oraux ou écrits, obtenus par l'Utilisateur de ORBE ou lors de l'utilisation des Services ne sont susceptibles de créer des garanties non expressément prévues par les présentes CGU.
            L'Utilisateur reconnaît être informé et accepte le fait que ORBE ne peut être, à aucun titre, tenue pour responsable de la création, de la modification, de la suppression, ou du défaut de réception, d'émission, de transmission ou de stockage de données des Utilisateurs, relevant de l'unique et entière responsabilité de ces derniers. De même, l'Utilisateur reconnaît que ORBE demeure libre de modifier à tout moment la dénomination de ses Services en ligne, sans que ceci constitue une modification des présentes et sans que cette modification puisse ouvrir droit à un quelconque recours de la part de l'Utilisateur.
            ORBE ne pourra en aucun cas être tenue pour responsable en raison d'une interruption d'un Service quelque soit la cause, la durée ou la fréquence de cette interruption.
            L'Utilisateur déclare et reconnaît être conscient que toutes les informations, données, textes, logiciels, musiques, sons, photographies, images, vidéos, messages ou tous autres contenus ou tous autres matériels (ci-avant et ci-après dénommés collectivement le « Contenu ») qu'ils soient portés à la connaissance du public ou transmis de manière privée, sont sous la seule responsabilité de la personne ayant émis ce Contenu. L'Utilisateur seul, et non ORBE, est entièrement responsable du Contenu qu'il affiche, télécharge, envoie ou transmet de quelque manière que ce soit par les Services. Sauf stipulation contraire expresse dans les présentes, ORBE n'exerce pas de contrôle du Contenu transmis via les Services et en conséquence ne garantit pas l'opportunité, la licéité, la probité ou la qualité de ce Contenu. En toutes hypothèses, ORBE ne pourra en aucun cas être tenue pour responsable du Contenu, notamment du caractère illégal du Contenu au regard de la réglementation en vigueur, d'erreur ou d'omission dans tout Contenu, de toute perte ou dommage consécutifs à l'utilisation de tout Contenu affiché, transmis via les Services. ORBE informe également les Utilisateurs des risques encourus par eux du fait d'actes de jeux réalisés en violation de la loi.
            De manière générale, l'Utilisateur s'engage à se conformer à l'ensemble des lois et réglementations en vigueur concernant l'interdiction de la diffusion d'images pornographiques, pédophiles, obscènes, ou de nature à porter gravement atteinte à la dignité humaine.<br/>

            Propriété intellectuelle et droits d'auteur
            ORBE est propriétaire exclusif de tous les droits de propriété intellectuelle sur la structure du site Web ou a acquis régulièrement les droits permettant l'exploitation du site Web, sans aucune limitation.
            ORBE concède à l'utilisateur, le droit d'utiliser le site Web et le Service pour ses besoins, à l'exclusion de toute utilisation lucrative.
            Sous réserve des droits concédés ci-dessus à l'utilisateur, il est notamment interdit de copier, reproduire, représenter, modifier et/ou exploiter, de quelque façon que ce soit et à quelque fin que ce soit, tout ou partie de la structure et du contenu du Service et du site Web.
            Tout téléchargement, dès lors qu'il n'est pas explicitement permis, est formellement interdit ; tout téléchargement abusif qui serait constaté pourrait entraîner de la part de ORBE et/ou de tout tiers concerné la prise de sanctions qui se révéleraient appropriées, notamment en cas de violation des droits de la propriété intellectuelle.
            Protection de la vie privée et des données à caractère personnel des Utilisateurs
            ORBE respecte les libertés fondamentales et en particulier le droit au respect de la vie privée des Utilisateurs. En application de la loi n°78-17 du 6 janvier 1978 modifiée par la loi n°2004-801 du 06 Août 2004 relative à l'informatique, aux fichiers et aux libertés, chaque Utilisateur ou non Utilisateur dispose des droits d'opposition, d'accès et de rectification des données le concernant. Il peut ainsi exiger que soient rectifiées, complétées, clarifiées, mises à jour ou effacées les informations le concernant qui sont inexactes, incomplètes, équivoques, ou périmées.
            À la clôture du Profil Utilisateur d'un Utilisateur, pour quelque cause que ce soit, les données relatives à ce compte, sont effacées. Cependant, conformément au Décret n° 2011-219 du 25 février 2011 relatif à la conservation et à la communication des données permettant d'identifier toute personne ayant contribué à la création d'un contenu mis en ligne (Cliquez ici pour consulter le décret), certaines données peuvent être conservées pour une durée maximale de un an.
            Le système permet d'ajouter les données personnelles relatives à un autre Membre dans le but de créer ue Communauté complète. Ceci doit être signalé car les limitations mentionnées ci-dessus sont applicables dans les deux sens, c'est à dire aux personnes ajoutant leurs propres références comme à celles qui ajoutent des références à d'autres personnes.
            Une personne non inscrite sur le Service pourra contacter ORBE afin de faire supprimer des données la concernant. Dans ce cas, une pièce d'identité pourra être demandée.
            Aucun utilisateur (enregistré ou non) ne doit violer le système de sécurité de ORBE. Toute tentative dans ce sens impliquera immédiatement le recours aux actions légales pertinentes tant civiles que pénales. Parmi les atteintes les plus fréquentes, il convient de mentionner sans être exhaustif :
            Tester ou évaluer la vulnérabilité du système ou du réseau.
            Violer les mesures de sécurité ou d'identification sans l'autorisation opportune.
            Accéder aux données non destinées à cet utilisateur ou accéder à ORBE à travers un compte ou un accès non autorisé.
            Empêcher ou tenter d'empêcher l'accès au service à tout utilisateur à travers quelque méthode que ce soit, ainsi qu'à travers un virus, la saturation du serveur et de son réseau, spams, envois massifs de courriels, comme les plus connus entre autres.
            Corrompre toute information ou courrier électronique, en falsifiant les en-têtes des messages ou de tout autre type de contenu.
            Toutefois, les utilisateurs de ORBE seront tenus à une série d'obligations qu'ils devront satisfaire à chaque instant en assumant l'entière responsabilité découlant du non-respect de celles-ci. Nous vous citons ci-après les plus significatives sans être exhaustifs :
            Obligation d'informer des données biographiques exactes conformes à la réalité (le contraire irait contre l'essence de la plate-forme).
            Obligation de ne pas révéler ni partager votre mot de passe avec des tiers ou utiliser votre mot de passe pour toute autre raison non autorisée.
            Obligation de ne pas utiliser le système aux fins de promotion, diffusion indiscriminée ou acquisition de membres dans le cadre de programmes et d'activités de type pyramidal, marketing multi niveau, ou systèmes affiliés, et ce via tous nos moyens de communication. ORBE se réserve le droit d'éliminer les annonces portant atteinte à cette règle et en cas d'abus, de supprimer le compte de l'utilisateur contrevenant.
            Obligation de n'annoncer à aucun moment un contenu comportant des droits d'auteur ou tout autre droit relatif à la propriété intellectuelle ou industrielle à moins qu'il s'agisse du propriétaire de ces droits ou de toute personne autorisée.
            Obligation de n'annoncer à aucun moment un contenu révélant ou pouvant révéler à un moment donné des secrets d'ordre commercial, enfreindre les droits de la propriété intellectuelle ou industrielle de tiers, ou les droits de confidentialité ou à l'image de tiers ; à moins qu'il s'agisse du propriétaire ou de toute personne autorisée.
            Obligation de n'annoncer à aucun moment un contenu obscène, diffamatoire, menaçant envers un autre utilisateur ou toute autre personne.
            Obligation d'être vous-même responsable de maintenir dans le secret vos données et votre mot de passe.
            ORBE se réserve le droit d'interdire l'accès à un utilisateur, en cas de violation des présentes conditions, pour une durée maximale de un an. Dans ce cas, l'utilisateur sera informé que son accès est interdit au service.
            La propriété du contenu apporté par l'utilisateur relèvera toujours de ce dernier. ORBE n'a pas le droit de l'utiliser, de le reproduire ou de le modifier sans accord préalable.
            Utilisation des cookies
            Afin d'améliorer l'interactivité et la convivialité de son site, ORBE stocke de petits fichiers sur le disque dur de l'ordinateur de l'utilisateur. Ces petits fichiers sont appelés cookies. ORBE ne stocke et n'envoie aucune information personnelle via ces fichiers.
            L'utilisateur est informé qu'il peut s'opposer à leur mise en place en suivant la procédure indiquée sur son navigateur mais que le site Internet de ORBE n'est pas exploitable sans autoriser leur utilisation.
            Garanties
            ORBE s'efforce dans la mesure du possible de maintenir accessible le Service 7 jours sur 7 et 24 heures sur 24, mais n'est tenu à aucune obligation d'y parvenir.
            ORBE peut donc interrompre l'accès pour toute raison, notamment technique. ORBE n'est en aucun cas responsable de ces interruptions et des conséquences qui peuvent en découler pour l'utilisateur ou tout tiers.
            Il est rappelé que ORBE peut mettre fin ou modifier les caractéristiques de ces services à tout moment, et cela sans préavis.
            Par ailleurs l'utilisateur déclare et garantit qu'il connaît parfaitement les caractéristiques et les contraintes de l'Internet, et notamment que les transmissions de données et d'informations sur l'Internet ne bénéficient que d'une fiabilité technique relative, celles-ci circulant sur des réseaux hétérogènes aux caractéristiques et capacités techniques diverses, qui perturbent l'accès ou le rendent impossibles à certaines périodes.
            Le service apporté par ORBE respecte la vie privée de ses utilisateurs (vous ne partagez uniquement aux Membres des Communautés auxquelles vous appartenez). Néanmoins, ORBE informe et met en garde l'utilisateur du risque d'atteinte à la vie privée dont il pourrait faire l'objet lorsqu'il dépose des données sur le site (notamment l'utilisation frauduleuse de ses données par un autre utilisateur du site).
            L'utilisateur accepte le fait que ORBE ou ses fournisseurs ne pourront en aucun cas être tenus responsables de tout dommage matériel et/ou immatériel et/ou direct et indirect de quelque nature que ce soit et/ou découlant de l'utilisation ou de l'impossibilité d'utiliser le Service.
            Il appartient à l'utilisateur de prendre toutes les mesures appropriées de façon à protéger ses propres données et ou logiciels de la contamination par d'éventuels virus circulants sur le réseau Internet.
            ORBE ne pourra être tenu responsable en cas de poursuites judiciaires à l'encontre de l'utilisateur et/ou de ses préposés du fait de l'usage du Service.
            L'utilisateur est responsable envers ORBE et/ou les tiers de tout préjudice matériel et/ou immatériel et/ou direct et indirect de quelque nature que ce soit causé par l'utilisateur et/ou ses préposés lors de l'utilisation par l'utilisateur du Service. En particulier, la responsabilité de ORBE ne saurait être engagée en cas de piratage total ou partiel du Site Web et des dommages que ce piratage pourrait entraîner à votre égard ou à l'égard d'un tiers.
            Le site Web présente des prestations proposées par des annonceurs et contient donc des données fournies par ces derniers quant à leurs produits et services. Il contient également des conseils et des informations qui peuvent contenir des inexactitudes, des omissions, des lacunes, des informations dépassées.
            ORBE décline toute responsabilité quant à ces contenus et ne garantit pas la véracité ni l'exhaustivité de ces informations. Ces informations n'engagent en rien la responsabilité de ORBE sur la qualité réelle ou supposée des produits ou des services annoncés sur le site.
            ORBE dégage toute responsabilité en cas d'utilisation du Service non conforme aux présentes.<br/>

            <h3>VII Informations légales</h3>

            Le site web “www.orbe…..(a prévoir)” est édité par M. Mehdi Peluffe, M. Amine Dai et M. Thomas Graveleine, en tant que particulier.<br/>

            Le site web est hébérgé par (à prévoir), www…..(à prévoir), dont le siége social est situé au (à prévoir).<br/>

            Orbe.(à prévoir) sont des domaines détenus par M. Mehdi Peluffe, M. Amine Dai et M. Thomas Graveleine.<br/>

            <h3>VIII Loi applicable</h3>

            Les présentes conditions sont soumises au droit Français.<br/>
            CNIL<br/>
            ORBE est déclaré à la CNIL (Commission Nationale de l'Informatique et des Libertés) sous le numéro (a prévoir) conformément à la loi du 6 Janvier 1978 relative à l'informatique, aux fichiers et aux libertés.<br/>


        </article>
    </div>
</div>
    <!-- fin login -->
    <!-- debut footer -->
    <footer>
    </footer>
    <!-- fin footer -->
    <!-- Chargement des fichiers javascript -->
    <script src="js/jquery-2.0.3.min.js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/flatui-checkbox.js"></script>
    <script src="js/flatui-radio.js"></script>
    <script src="js/jquery.tagsinput.js"></script>
    <script src="js/jquery.placeholder.js"></script>
    <script src="js/application.js"></script>
</body>
</html>
